var express = require('express');
var app = express();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
var res = "";


//Setup simple node.js express server on port 3000

app.set('port', process.env.PORT || 3000);
app.use(express.static(__dirname+'/public'));

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

http.listen(app.get('port'), function(){
  console.log('listening on *:'+app.get('port'));
});

//Setup socket between static site and server
io.on('connection', function(socket){
  console.log('a user connected');
  socket.emit('udp', res);
  socket.on('disconnect', function(){
    console.log('user disconnected');
  });
});

//Setup the UDP receiver
var dgram = require('dgram');
var srv = dgram.createSocket("udp4");

//On received UDP message - forward it through the socket
srv.on("message", function (msg, rinfo) {
  msg = msg.toString("ascii");
  msg = msg.replace(";","");
  res = msg.split(" ");
  io.emit('udp', res);
});

srv.on("listening", function () {
  var address = srv.address();
  console.log("server listening " + address.address + ":" + address.port);
});

srv.on('error', function (err) {
  console.error(err);
  process.exit(0);
});

//Setup server to listen for UDP messages on port 60810
srv.bind(60810);