# README #

This site is based on a node.js built UDP socket to transmit data received via UDP in real-time to a webpage.

## Requirements ##
* Server with node.js installed
* A static IP for the node.js server

## Installation ##
* Change IP in index.html (line 92) to the servers static IP

### Dependencies ###
* socket.IO
* express

### Port Number ###
The server listens for UDP messages on port 60810